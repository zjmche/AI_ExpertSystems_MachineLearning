# rule_fc.py

from pyke import contexts, pattern, fc_rule, knowledge_base

pyke_version = '1.1.1'
compiler_version = 1

def direct_father_son(rule, context = None, index = None):
  engine = rule.rule_base.engine
  if context is None: context = contexts.simple_context()
  try:
    with knowledge_base.Gen_once if index == 0 \
             else engine.lookup('family', 'son_of', context,
                                rule.foreach_patterns(0)) \
      as gen_0:
      for dummy in gen_0:
        engine.assert_('family', 'father_son',
                       (rule.pattern(0).as_data(context),
                        rule.pattern(1).as_data(context),
                        rule.pattern(2).as_data(context),)),
        rule.rule_base.num_fc_rules_triggered += 1
  finally:
    context.done()

def grand_father_son(rule, context = None, index = None):
  engine = rule.rule_base.engine
  if context is None: context = contexts.simple_context()
  try:
    with knowledge_base.Gen_once if index == 0 \
             else engine.lookup('family', 'father_son', context,
                                rule.foreach_patterns(0)) \
      as gen_0:
      for dummy in gen_0:
        with knowledge_base.Gen_once if index == 1 \
                 else engine.lookup('family', 'father_son', context,
                                    rule.foreach_patterns(1)) \
          as gen_1:
          for dummy in gen_1:
            engine.assert_('family', 'father_son',
                           (rule.pattern(0).as_data(context),
                            rule.pattern(1).as_data(context),
                            rule.pattern(2).as_data(context),)),
            rule.rule_base.num_fc_rules_triggered += 1
  finally:
    context.done()

def populate(engine):
  This_rule_base = engine.get_create('rule')
  
  fc_rule.fc_rule('direct_father_son', This_rule_base, direct_father_son,
    (('family', 'son_of',
      (contexts.variable('son'),
       contexts.variable('father'),),
      False),),
    (contexts.variable('father'),
     contexts.variable('son'),
     pattern.pattern_literal(()),))
  
  fc_rule.fc_rule('grand_father_son', This_rule_base, grand_father_son,
    (('family', 'father_son',
      (contexts.variable('father'),
       contexts.variable('grand_son'),
       pattern.pattern_literal(()),),
      False),
     ('family', 'father_son',
      (contexts.variable('grand_father'),
       contexts.variable('father'),
       pattern.pattern_literal(()),),
      False),),
    (contexts.variable('grand_father'),
     contexts.variable('grand_son'),
     pattern.pattern_literal(('grand',)),))


Krb_filename = '..\\rule.krb'
Krb_lineno_map = (
    ((12, 16), (3, 3)),
    ((17, 20), (5, 5)),
    ((29, 33), (9, 9)),
    ((34, 38), (10, 10)),
    ((39, 42), (12, 12)),
)
