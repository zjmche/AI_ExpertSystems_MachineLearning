# -*- coding: utf-8 -*-
"""
Created on Sat Nov 27 21:09:20 2021

@author: JZ
"""

import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from matplotlib.collections import LineCollection
from matplotlib.colors import ListedColormap, BoundaryNorm


def threshold_plot(ax, x, y, threshv, color, overcolor):
	"""
	Helper function to plot points above a threshold in a different color
	From https://stackoverflow.com/questions/30121773/python-is-it-possible-to-change-line-color-in-a-plot-if-exceeds-a-specific-range

	Args:
		ax : Axes
			Axes to plot to
		x, y : array
			The x and y values

		threshv : float
			Plot using overcolor above this value

		color : color
			The color to use for the lower values

		overcolor: color
			The color to use for values over threshv
	"""
	
	# Create a colormap for red, green and blue and a norm to color
	# f' < -0.5 red, f' > 0.5 blue, and the rest green
	cmap = ListedColormap([color, overcolor])
	norm = BoundaryNorm([np.min(y), threshv, np.max(y)], cmap.N)

	if threshv > max(y): # if none of the values are above threshv
		cmap = ListedColormap([color])
		norm = BoundaryNorm([np.min(y), np.max(y)], cmap.N)

	# Create a set of line segments so that we can color them individually
	# This creates the points as a N x 1 x 2 array so that we can stack points
	# together easily to get the segments. The segments array for line collection
	# needs to be numlines x points per line x 2 (x and y)
	points = np.array([x, y]).T.reshape(-1, 1, 2)
	segments = np.concatenate([points[:-1], points[1:]], axis=1)

	# Create the line collection object, setting the colormapping parameters.
	# Have to set the actual values used for colormapping separately.
	lc = LineCollection(segments, cmap=cmap, norm=norm)
	lc.set_array(y)

	ax.add_collection(lc)
	ax.set_xlim(np.min(x), np.max(x))
	ax.set_ylim(np.min(y) * 1.1, np.max(y) * 1.1)
	return lc



def calculate_ucl(in_control_stats, false_positive_rate, step_size=0.01):
	"""
	Given a target false positive rate, this method will automatically calculate
	an Upper Control Limit (UCL). This method should likely only be run with larger
	datasets for preliminary testing purposes.

	Args:
		in_control_stats: an array-like list of calulated statistic values (i.e. 
			from an MSPC method)
		false_positive_rate: the desired false positive rate
		step_size: the amount the UCL will be lowered each iteration until 
			``false_positive_rate`` is reached. Adjusting this will alter the number
			of iterations for convergence and might slightly decrease accuracy. 
	"""
	ucl = max(in_control_stats)
	ucl += step_size
	count = 0

	while(count < (false_positive_rate*len(in_control_stats))):
		ucl = ucl - step_size # lower UCL by step_size
		# count number of statistics above current ucl
		count = len([i for i in in_control_stats if i > ucl])
	return ucl


def mcusum(df, num_in_control, k, alpha=0, plotting=True, save='', plot_title='MCUSUM'):
	"""
	Implementation of the Multivariate Cumulative Sum (MCUSUM) method.

		Based on Kent(2007) https://etd.ohiolink.edu/rws_etd/send_file/send?accession=kent1185558637&disposition=inline

		- Reference 17 : (Jackson 1985)
		
		- Reference 5  : (Crosier 1988)

	Args:
		df: multivariate dataset as Pandas DataFrame
		num_in_control: number of in control observations
		k: the slack parameter which determines model sensetivity (should typically be set to 1/2 of the mean shift that you expect to detect)
		alpha: the percentage of false positives we want to allow, used for calculating the Upper Control Limit
		save: the directory to save the graphs to, if not changed from default, nothing will be saved
		plot_title: the title for the plot generated
	Returns:
		MCUSUM statistic values and a calculated UCL with approximately ``alpha`` false positive rate
	"""
	a = df.head(num_in_control).mean(axis=0)  # mean vector of in control data
	cov_inv = np.linalg.inv(np.cov(df.head(num_in_control), rowvar=False,bias=True))  # covariance matrix inverted
	s_old = [0] * df.shape[1]
	y_vals = [0] * df.shape[0]
	for n in range(0, df.shape[0]):
		# get current line
		x = df.iloc[n]

		# calculate Cn
		tf = np.array(s_old + x - a)
		c = np.matmul(np.matmul(tf, cov_inv), tf)
		c = math.sqrt(c)

		# calculate kv (vector k)
		kv = (k / c) * tf

		# calculate new s
		if (c <= k):
			s_old = 0
		else:
			s_old = tf * (1 - (k / c))
			# calculate y (new c)
			tf = np.array(s_old + x - a)
			y = np.matmul(np.matmul(tf, cov_inv), tf)
			y = math.sqrt(y)
			y_vals[n] = y
	
	#calculate UCL
	in_stats = y_vals[:num_in_control]
 
	ucl = calculate_ucl(in_stats, alpha)
	
	#plotting
	if plotting:
		plt.style.use('ggplot')
		fig, ax = plt.subplots(figsize=(10, 7))
		lc = threshold_plot(ax, range(0, df.shape[0]), np.array(y_vals), ucl, 'b', 'r')
		ax.axhline(ucl, color='k', ls='--')
		ax.set_title(plot_title)
		ax.set_xlabel('Observation Number')
		ax.set_ylabel('MCUSUM statistic (Anomaly Score)')

		#saving plot
		if save != "":
			if save[-1] != '/':
				save += '/'
			plt.savefig(save + plot_title + '_FULL.png', dpi=300)
			
			#if there are a lot of entries, a smaller graph will also be saved that
			#shows only the end of the graph for more detail.
			if len(y_vals) > 10000:
				ax.set_xlim(9000)
				plt.savefig(save + plot_title + '_SMALL.png', dpi=300)
			
		
	#returns the mcusum statistics as a list and upper control limit
	return y_vals, ucl


def mewma(df, num_in_control, lambd=0.1, alpha=0, plotting=True, save='', plot_title='MEWMA'):
	"""
	Args:
		df: multivariate dataset as Pandas DataFrame
		num_in_control: number of rows before anomalies begin
		lambd: smoothing parameter between 0 and 1; lower value = higher weight to older observations; default is 0.1
		alpha: the percentage of false positives we want to allow, used for calculating the Upper Control Limit
		save: the directory to save the graphs to, if not changed from default, nothing will be saved
		plot_title: the title for the plot generated
	Returns:
		MEWMA statistic values and a calculated UCL with approximately ``alpha`` false positive rate
	"""
	nrow, ncol = df.shape
	means = df.head(num_in_control).mean(axis=0)

	# create diff matrix
	v = np.zeros(shape=(nrow - 1, ncol))
	for i in range(nrow - 1):
		v[i] = df.iloc[i + 1] - df.iloc[i]

	# calculate vTv
	vtv = v.T @ v

	# calculate S matrix
	S = (1 / (2 * (nrow-1))) * (vtv)

	mx = df - means

	# calculate z
	z = np.zeros(shape=(nrow + 1, ncol))
	for i in range(nrow):
		z[i + 1] = lambd * mx.iloc[i] + (1 - lambd) * z[i]
	z = z[1:, :]

	mewma_stats = []  # test statistic values
	for i in range(nrow):
		w = (lambd / (2 - lambd)) * (1 - (1 - lambd)**(2 * (i + 1)))
		inv = np.linalg.inv(w * S)
		mewma_stats.append((z[i].T @ inv) @ z[i])
		
	#calculate UCL
	in_stats = mewma_stats[:num_in_control]
	ucl = calculate_ucl(in_stats, alpha)
	
	#plotting
	if plotting:
		plt.style.use('ggplot')
		fig, ax = plt.subplots(figsize=(10, 7))
		threshold_plot(ax, range(0, df.shape[0]), np.array(mewma_stats), ucl, 'b', 'r')
		ax.axhline(ucl, color='k', ls='--')
		ax.set_title(plot_title)
		ax.set_xlabel('Observation Number')
		ax.set_ylabel('MEWMA statistic (Anomaly Score)')

		#saving plot
		if save != "":
			if save[-1] != '/':
				save += '/'
			plt.savefig(save + plot_title + '_FULL.png', dpi=300)
			
			#if there are a lot of entries, a smaller graph will also be saved that
			#shows only the end of the graph for more detail.
			if df.shape[0] > 10000:
				ax.set_xlim(9000)
				plt.savefig(save + plot_title + '_SMALL.png', dpi=300)

	# return MEWMA statistic values and calculated UCL
	return mewma_stats, ucl


def pca_mewma(df, num_in_control, num_princ_comps, alpha=0, lambd=0.1, plotting=True,
	save='', plot_title='PC_MEWMA'):
	"""
	MEWMA on Principle Components
	Variables contained in ``df`` must have mean 0

	Args:
		df: multivariate dataset as Pandas DataFrame
		num_in_control: number of in control observations
		num_princ_comps: number of principle components to include
		alpha: the percentage of false positives we want to allow, used for calculating the Upper Control Limit
		lambd: smoothing parameter between 0 and 1; lower value = higher weightage to older observations; default is 0.1
		save: the directory to save the graphs to, if not changed from default, nothing will be saved
		plot_title: the title for the plot generated
	Returns:
		MEWMA statistic values using PCA for dimensionality reduction and a calculated UCL with approximately ``alpha`` false positive rate
	"""
	df = df-df.mean()
	pca = PCA(n_components=num_princ_comps)
	s = pca.fit(df)
	eigvec_mat = s.components_ # V transpose
	W_matrix = []  # since apply_mewma only takes a df, not observations as they happen
	for index, row in df.iterrows():
		W = eigvec_mat @ row
		W_matrix.append(W)
	W_df = pd.DataFrame(W_matrix)
	return mewma(W_df,
					   num_in_control,
					   lambd=lambd,
					   alpha=alpha,
					   plotting=plotting,
					   save=save,
					   plot_title=plot_title)
