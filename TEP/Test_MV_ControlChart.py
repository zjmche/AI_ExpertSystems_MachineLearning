# -*- coding: utf-8 -*-
"""
Created on Thu Nov 25 19:30:39 2021

@author: JZ
"""

import numpy as np
import pandas as pd
from scipy.stats import norm, chi2
from scipy import stats

import PCA_EwmaCusum as PEC

import matplotlib.pyplot as plt

#生成正常数据
num_sample=100
a = 10*np.random.randn(num_sample, 1)
x1 = a + np.random.randn(num_sample, 1)
x2 = 1*np.sin(a) + np.random.randn(num_sample, 1)
x3 = 5*np.cos(5*a) +np.random.randn(num_sample, 1)
x4 = 0.8*x2 + 0.1*x3+np.random.randn(num_sample, 1)
x = np.c_[x1,x2,x3,x4]
xx_train=x

#产生测试集
a = 10*np.random.randn(num_sample, 1)
x1 = a + np.random.randn(num_sample, 1)
x2 = 1*np.sin(a) + np.random.randn(num_sample, 1)
x3 = 5*np.cos(5*a) +np.random.randn(num_sample, 1)
x4 = 0.8*x2 + 0.1*x3+np.random.randn(num_sample, 1)
xx_test = np.c_[x1,x2,x3,x4]
xx_test[50:,1]=xx_test[50:,1]+15


Xtrain =pd.DataFrame(xx_train)
Xtest =pd.DataFrame(xx_test)

Xtrain.plot()
Xtest.plot()

PEC.pca_mewma(df=Xtrain, num_in_control=100,num_princ_comps=3, alpha=0.9, 
               lambd=0.1, plotting=True, plot_title='PC_MEWMA')

PEC.mewma(df=Xtrain, num_in_control=100, alpha=0.9, 
               lambd=0.1, plotting=True, plot_title='Original_MEWMA')

PEC.pca_mewma(df=Xtest, num_in_control=100,num_princ_comps=3, alpha=0.9, 
               lambd=0.1, plotting=True, plot_title='PC_MEWMA')

PEC.mewma(df=Xtest, num_in_control=100, alpha=0.9, 
               lambd=0.1, plotting=True, plot_title='Original_MEWMA')




PEC.mcusum(df=Xtrain, num_in_control=100, k=0.5, alpha=0, plotting=True, 
             plot_title='MCUSUM')

PEC.mcusum(df=Xtest, num_in_control=100, k=0.5, alpha=0, plotting=True, 
             plot_title='MCUSUM')

