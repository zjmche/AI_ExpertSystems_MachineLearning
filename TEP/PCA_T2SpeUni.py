##原文链接：https://blog.csdn.net/weixin_40418080/article/details/120186027

import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from scipy import stats
from scipy.stats import norm, chi2
from sklearn.preprocessing import StandardScaler


plt.rcParams['font.sans-serif']=['SimHei'] #用来正常显示中文标签
plt.rcParams['axes.unicode_minus'] = False #用来正常显示负号


# 在训练出 负载矩 P 及 特征值对角矩 V后，单一点的T^2值
def t2_online(x_vec, p_mat, v_mat):
    '''
    p：特征向量组成的降维矩阵，负载矩阵
    x：在线样本，shape为m*1
    v：特征值由大至小构成的对角矩阵
    '''
    T_2 = np.dot(np.dot((np.dot((np.dot(x_vec.T, p_mat)), 
                                np.linalg.inv(v_mat))), p_mat.T), x_vec)
    return T_2

# 在训练出 负载矩 P 及 特征值对角矩 V后，单一点的SPE值
def spe_online(x_vec, p_mat):
    '''
    p：特征向量组成的降维矩阵，负载矩阵
    x：在线样本，shape为m*1
    '''
    I_mat = np.eye(len(x_vec))
    spe = np.dot(np.dot(x_vec.T, I_mat - np.dot(p_mat, p_mat.T)), x_vec)
    
    ## Q_count = np.linalg.norm(np.dot((I - np.dot(p_k, p_k.T)), 
    ## test_data_nor), ord=None, axis=None, keepdims=False)  #二范数计算方法
    return spe

def uni_online(x_vec,p_mat,v_mat,t2_UCL, spe_UCL):
    t2=t2_online(x_vec,p_mat,v_mat)
    spe=spe_online(x_vec,p_mat)
    uni=(spe/spe_UCL)+(t2/t2_UCL)
    return uni
    

def pca_control_limit(Xtrain, ratio = 0.95, confidence = 0.99):
    '''
    计算出T2和SPE统计量
    '''
    pca = PCA(n_components = ratio)
    pca.fit(Xtrain)
    
    evr = pca.explained_variance_ratio_
    ev = pca.explained_variance_ # 方差，相当于X的协方差的最大的前几个特征值

    p_mat = (pca.components_).T # 负载矩阵
    v_mat = np.diag(ev) # 特征值组成的对角矩阵
    v_all = PCA(n_components = Xtrain.shape[1]).fit(Xtrain).explained_variance_
    p_all = (PCA(n_components = Xtrain.shape[1]).fit(Xtrain).components_).T
    k = len(evr)
    n_sample = pca.n_samples_
    
    ##T统计量阈值计算
    coe = k* (n_sample - 1) * (n_sample + 1) / ((n_sample - k) * n_sample)
    t2_UCL = coe * stats.f.ppf(confidence, k, (n_sample - k))

    ##SPE统计量阈值计算
    theta1 = np.sum((v_all[k:]) ** 1)
    theta2 = np.sum((v_all[k:]) ** 2)
    theta3 = np.sum((v_all[k:]) ** 3)
    h0 = 1 - (2 * theta1 * theta3) / (3 * (theta2 ** 2))
    c_alpha = norm.ppf(confidence)
    spe_UCL = theta1 * ((h0 * c_alpha * ((2 * theta2) ** 0.5)
                           / theta1 + 1 + theta2 * h0 * (h0 - 1) / (theta1 ** 2)) ** (1 / h0))
   
    
    fai=np.matmul(np.matmul(p_mat,v_mat),p_mat.T)/t2_UCL 
    fai=fai + (np.eye(len(p_mat))-np.matmul(p_mat,p_mat.T)) / spe_UCL
    
    g=np.trace(np.square(np.matmul(Xtrain,fai)))/np.trace(np.matmul(Xtrain,fai))
    h=(np.trace(np.matmul(Xtrain,fai)))**2/np.trace(np.square(np.matmul(Xtrain,fai)))

    uni_UCL =g*chi2.ppf(confidence,h)
    return t2_UCL, spe_UCL, uni_UCL, p_mat, v_mat, v_all, k, p_all

def pca_model_online(X_mat, p_mat, v_mat):
    t2_all = []
    spe_all = []
    for x in range(np.shape(X_mat)[0]):
        data_in = X_mat[x]
        t = t2_online(data_in, p_mat, v_mat)
        q = spe_online(data_in, p_mat)
        t2_all.append(t)
        spe_all.append(q)
    return t2_all, spe_all

def figure_control_limit(X, t2_UCL, spe_UCL, uni_UCL, t2_all, spe_all):
    ## 画控制限的图
    plt.figure(2, figsize=(12, 7))
    ax1 = plt.subplot(3, 1, 1)
    plt.plot(t2_all)
    plt.plot(np.ones((len(X))) * t2_UCL, 'r', label='95% $T^2$ control limit')
    # ax1.set_ylim(0,100)
    # plt.xlim(0,100)
    ax1.set_xlabel(u'Samples')
    ax1.set_ylabel(u'Hotelling $T^2$ statistic')
    plt.legend()

    ax2 = plt.subplot(3, 1, 2)
    plt.plot(spe_all)
    plt.plot(np.ones((len(X))) * spe_UCL, 'r', label='95% spe control limit')
    # ax1.set_ylim(0,30)
    # plt.xlim(0,100)
    ax2.set_xlabel(u'Samples')
    ax2.set_ylabel(u'SPE statistic')
    plt.legend()
    
    ax3 = plt.subplot(3, 1, 3)
    uni_all=(spe_all/spe_UCL)+(t2_all/t2_UCL)
    
    plt.plot(uni_all)
    plt.plot(np.ones((len(X))) * uni_UCL, 'r', label='95% spe control limit')
    # ax1.set_ylim(0,30)
    # plt.xlim(0,100)
    ax3.set_xlabel(u'Samples')
    ax3.set_ylabel(u'Uni statistic')
    plt.legend()
    plt.legend()
    
    plt.show()

def Contribution_graph(test_data, Xtrain_nor, index, p, p_all, v_all, k, t2_UCL):
    # 贡献图
    index = 160
    #1.确定造成失控状态的得分
    #test_data = fault02_test
    data_mean = data_mean = np.mean(Xtrain_nor, 0)
    data_std = np.std(Xtrain_nor, 0)
    test_data_submean = np.array(test_data - data_mean)
    test_data_norm = np.array((test_data - data_mean) / data_std)
    t = test_data_norm[index,:].reshape(1,test_data.shape[1])
    S = np.dot(t,p[:,:])
    r = []
    for i in range(k):
        if S[0,i]**2/v_all[i] > t2_UCL/k:
            r.append(i)
    print(r)
    #2.计算每个变量相对于上述失控得分的贡献
    cont = np.zeros([len(r),test_data.shape[1]])
    for i in range(len(r)):
        for j in range(test_data.shape[1]):
            cont[i,j] = S[0,i]/v_all[r[i]]*p_all[r[i],j]*test_data_submean[index,j]
            if cont[i,j] < 0:
                cont[i,j] = 0
    #3.计算每个变量对T的总贡献
    a = cont.sum(axis = 0)
    #4.计算每个变量对Q的贡献
    I = np.eye(test_data.shape[1])
    e = (np.dot(test_data_norm[index,:],(I - np.dot(p, p.T))))**2

    ##画图
    plt.rcParams['font.sans-serif']=['SimHei']
    plt.rcParams['axes.unicode_minus'] = False
    font1 = {'family' : 'SimHei','weight' : 'normal','size'   : 23,}
    plt.figure(2,figsize=(16,9))
    plt.subplot(2,1,1)
    plt.bar(range(test_data.shape[1]),a)
    plt.xlabel(u'变量号',font1)
    plt.ylabel(u'T^2贡献率 %',font1)
    plt.legend()
    plt.show
    plt.subplot(2,1,2)
    plt.bar(range(test_data.shape[1]),e)
    plt.xlabel(u'变量号',font1)
    plt.ylabel(u'Q贡献率 %',font1)
    plt.legend()
    plt.show()


